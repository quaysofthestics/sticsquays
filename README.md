# Socker

## Using the rocker/rstudio/stics container

### Quickstart

```
docker run -e PASSWORD=whateverpasswordyouset --rm -p 8787:8787 registry.forgemia.inra.fr/quaysofthestics/sticsquays/rstudio_stics:9.2
```

Visit [localhost:8787](http://localhost:8787) in your browser and log in with _username_ **rstudio** and the _password_ **whateverpasswordyouset**. NB: Setting a password is now REQUIRED. Container will error otherwise.

### Sharing volumes

For instance, if you want to share IO datas between your personal computer and the container.

Assuming you have a folder here `$(pwd)/sharedFolder`, you can launch the conatiner this way:

```
docker run -e PASSWORD=whateverpasswordyouset -e USERID=$(id -u $(whoami)) -e GROUPID=$(id -g $(whoami)) --rm -p 8787:8787 -v $(pwd)/sharedFolder:/home/rstudio/sharedFolder registry.forgemia.inra.fr/quaysofthestics/sticsquays/rstudio_stics:9.2
```
